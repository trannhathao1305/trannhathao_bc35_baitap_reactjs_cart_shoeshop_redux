import React, { Component } from "react";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import Cart from "./Cart";

export default class ShoeShopRedux extends Component {
  render() {
    return (
      <div className="container py-5">
        <Cart />
        <ListShoe />
        <DetailShoe />
      </div>
    );
  }
}
