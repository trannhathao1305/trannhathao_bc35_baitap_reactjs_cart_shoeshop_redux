import React, { Component } from "react";
import { connect } from "react-redux";
import {
  addToCartAction,
  changeDetailAction,
} from "./Redux/actions/shoeAction";

class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.data;
    return (
      <div className="col-3 p-1">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt="" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h2 className="card-title">{price} $</h2>
          </div>
          <div className="text-center">
            <button
              onClick={() => {
                this.props.handleChangeDetail(this.props.data);
              }}
              className="btn btn-secondary"
            >
              Show details
            </button>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
              className="btn btn-danger ml-1"
            >
              Choose to buy
            </button>
          </div>
        </div>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (shoe) => {
      dispatch(addToCartAction(shoe));
    },
    handleChangeDetail: (shoe) => {
      dispatch(changeDetailAction(shoe));
    },
  };
};
export default connect(null, mapDispatchToProps)(ItemShoe);
