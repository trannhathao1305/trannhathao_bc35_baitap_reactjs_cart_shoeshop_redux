import React, { Component } from "react";
import { connect } from "react-redux";
import { tangGiamSoLuongAction } from "./Redux/actions/shoeAction";

class Cart extends Component {
  renderTbody = () => {
    return this.props.gioHang?.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img style={{ width: "50px" }} src={item.image} alt="" />
          </td>
          <td>{item.price * item.number} $</td>
          <td>
            <button
              onClick={() => {
                this.props.handleTangGiamSoLuong(item.id, -1);
              }}
              className="btn btn-danger"
            >
              -
            </button>
            <span className="mx-2">{item.number}</span>
            <button
              onClick={() => {
                this.props.handleTangGiamSoLuong(item.id, +1);
              }}
              className="btn btn-success"
            >
              +
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <td>Id</td>
            <td>Name</td>
            <td>Image</td>
            <td>Price</td>
            <td>Quantity ( số lượng )</td>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeReducer.cart,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleTangGiamSoLuong: (id, soLuong) => {
      dispatch(tangGiamSoLuongAction(id, soLuong));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
